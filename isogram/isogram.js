export const isIsogram = input => {

	// convert to lowercase and remove non-alpha characters
	input = input.toLowerCase().replace(/\W/g, '');

	// string split on a unique letter produces exactly 2 segments
	for (const c of input)
		if (input.split(c).length !== 2)
			return false;

	return true;
};
