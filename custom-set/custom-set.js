export class CustomSet {
	constructor(numbers=[]) {
		this.set = numbers.filter((v, i, a) => a.indexOf(v) === i).sort();
	}

	empty() {
		return this.set.length < 1;
	}

	contains(number) {
		return this.set.includes(number);
	}

	subset(customSet) {
		return this.set.every(i => customSet.set.includes(i));
	}

	disjoint(customSet) {
		return this.set.every(i => !customSet.set.includes(i));
	}

	eql(customSet) {
		return this.set.every(i => customSet.set.includes(i))
			&& customSet.set.every(i => this.set.includes(i));
	}

	add(number) {
		if (!this.set.includes(number)) this.set.push(number);

		return this;
	}

	intersection(other) {
		return new CustomSet(this.set.filter(value => other.contains(value)));
	}

	difference(other) {
		return new CustomSet(this.set.filter(value => !other.contains(value)));
	}

	union(customSet) {
		const merged = new CustomSet(this.set);
		customSet.set.forEach(value => merged.add(value));

		return merged;
	}
}
