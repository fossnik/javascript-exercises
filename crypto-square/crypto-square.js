export class Crypto {
	constructor(input) {
		this.input = input;
	}

	normalizePlaintext() {
		return this.input.replace(/\W/g, '').toLowerCase();
	}

	size() {
		// width should be at least as long as the height
		return Math.ceil(Math.sqrt(this.normalizePlaintext(this.input).length));
	}

	plaintextSegments() {
		const text = this.normalizePlaintext(this.input);
		const width = this.size();
		let remit = [];

		for (let i = 0; i < text.length; i += width)
			remit.push(text.substr(i, width));

		return remit;
	}

	ciphertext() {
		const pTextSegments = this.plaintextSegments(this.input);
		const size = this.size();

		let remit = '';
		for (let row = 0; row < size; row++)
			for (let column = 0; column < pTextSegments.length; column++)
				remit += pTextSegments[column].charAt(row);

		return remit;
	}
}
