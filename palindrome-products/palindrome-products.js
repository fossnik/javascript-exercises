function generate({minFactor=1, maxFactor}) {
  const largest = {
    value: null,
    factors: new Set()
  },
  smallest = Object.assign({}, largest);

  let factorA = minFactor, factorB;
  while(factorA++ < maxFactor) {
    factorB = minFactor;
    while(factorB++ < maxFactor) {
      const product = factorA * factorB;
      if (product.toString() === product.toString().split('').reverse().join('')) {
        if (largest.value === null || largest.value < product) {
          largest.value = product;
          largest.factors.add([factorA, factorB].sort().toString());
        }
        if (smallest.value === null || smallest.value > product) {
          smallest.value = product;
          smallest.factors.add([factorA, factorB].sort().toString());
        }
      }
    }
  }

  // filter out smaller than the largest pairs
  largest.factors = Array.from(largest.factors)
    .map(e => Array.from(e.split(',')))
    .filter(factors => eval(factors.join('*')) === largest.value)[0];

  smallest.factors = [...smallest.factors]
    .map(e => [...e.split(',')])
    .filter(factors => eval(factors.join('*')) === smallest.value)[0];

  largest.factors = [Number(largest.factors[0]), Number(largest.factors[1])];
  smallest.factors = [Number(smallest.factors[1]), Number(smallest.factors[0])];

  return {
    largest,
    smallest
  }
}

export default generate;
