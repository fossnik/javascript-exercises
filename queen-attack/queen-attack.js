export class QueenAttack {
	constructor(setup) {
		if (setup) {
			if (setup.white[0] === setup.black[0]
			 && setup.white[1] === setup.black[1]) {
				throw new Error('Queens cannot share the same space');
			}
			this.white = setup.white;
			this.black = setup.black;
		} else {
			this.white = [0, 3];
			this.black = [7, 3];
		}
	}

	canAttack() {
		if (this.black[0] === this.white[0]) return true;  // same row
		if (this.black[1] === this.white[1]) return true;  // same column

		const rowDifference = Math.abs(this.black[0] - this.white[0]);
		const colDifference = Math.abs(this.black[1] - this.white[1]);

		return rowDifference === colDifference;
	}

	toString() {
		return [ 0, 1, 2, 3, 4, 5, 6, 7 ]
			.map(row =>
				[ 0, 1, 2, 3, 4, 5, 6, 7 ]
					.map(col => {
						if (this.black[0] === row && this.black[1] === col) return 'B';
						if (this.white[0] === row && this.white[1] === col) return 'W';
						return "_";
					})
					.join(" "))
			.join("\n") + "\n";
	}
}
