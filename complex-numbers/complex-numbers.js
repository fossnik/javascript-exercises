export class ComplexNumber {
	constructor(real, imag) {
		[this.real, this.imag] = [real, imag];
	}

	add({real, imag}) {
		return {
			real: this.real + real,
			imag: this.imag + imag
		}
	}

	sub({real, imag}) {
		return {
			real: this.real - real,
			imag: this.imag - imag
		}
	}

	mul({real, imag}) {
		return {
			real: this.real * real - this.imag * imag,
			imag: this.real * imag + this.imag * real
		}
	}

	div({real, imag}) {
		return {
			real: (this.real * real + this.imag * imag) / (real ** 2 + imag ** 2),
			imag: (this.imag * real - this.real * imag) / (real ** 2 + imag ** 2)
		}
	}

	get abs() {
		return Math.sqrt(this.real ** 2 + this.imag ** 2);
	}

	get conj() {
		return {
			real: Math.abs(this.real),
			imag: this.imag && -Math.abs(this.imag) || 0
		}
	}

	get exp() {
		return {
			real: Math.exp(this.real) * Math.cos(this.imag),
			imag: Math.exp(this.real) * Math.sin(this.imag)
		}
	}
}
