const values = {
	 1: ['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'],
	 2: ['D', 'G'],
	 3: ['B', 'C', 'M', 'P'],
	 4: ['F', 'H', 'V', 'W', 'Y'],
	 5: ['K'],
	 8: ['J', 'X'],
	10: ['Q', 'Z'],
};

const getValue = letter => Number(
		Object.entries(values)
			.filter(entry => entry[1].includes(letter))
			   .map(entry => entry[0]).toString());

export const score = (input) => eval(
	input
		.toUpperCase()
		.split('')
		.map(letter => getValue(letter))
		.join('+')
) || 0;
