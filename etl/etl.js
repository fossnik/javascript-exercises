export const transform = (input) => {
	let letterToValueMapping = {};

	/**
	 * this is definitely a little confusing
	 * here is how it breaks down though:
	 * {
	 * 1: ['A', 'E'],
	 * 2: ['D', 'G']
	 * }
	 *
	 * first entry:
	 *   entry[0]: "1"        // this is the point value
	 *   entry[1]: ["A", "E"] // these are the letters which have that value
	 * second entry:
	 *   entry[0]: "2"
	 *   entry[1]: ["D", "G"]
	 */

	Object.entries(input)
		.map(entry =>           // map through each entry
			entry[1].map(letter => // map through each letter contained in an entry
				letterToValueMapping[letter.toLowerCase()] = Number(entry[0])));

	return letterToValueMapping;
};
