export class Diamond {
	makeDiamond(letter) {
		const asciiOffset = 'A'.charCodeAt(0);
		const length = letter.codePointAt(0) - asciiOffset + 1;

		// Alphabet Array (input 'D'; output ['A', 'B', 'C', 'D'])
		const arr = Array
			.from({length},(remit='', index) =>
				remit + String.fromCharCode(index + asciiOffset));

		function getLine(index, char) {
			const outer = ' '.repeat(length - index - 1);
			const inner = ' '.repeat(index * 2).substr(1);

			return char === 'A'
				? outer + char + outer
				: outer + char + inner + char + outer;
		}

		// create the top part of the diamond
		const top = arr
			.reduce((remit, char, index) => [
					...remit,
					getLine(index, char)
				], []);

		// drop last row to avoid duplicate in middle
		top.pop();

		// create the bottom part of the diamond
		const bottom = arr
			.reduce((remit, char, index) => [
					getLine(index, char),
					...remit
				], []);

		return top.concat(bottom).join('\n') + '\n';
	}
}
