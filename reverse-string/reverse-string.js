export const reverseString = (input) => {
	let output = '';
	input.split("").map(value => output = value + output);
	return output;
};
