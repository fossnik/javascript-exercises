export class Luhn {
	constructor(input) {
		this.valid = Luhn.testValidity(input);
	}

	static testValidity(input) {
		if (input.length <= 1)
			return false;

		// only digits and spaces are valid
		if (input.match(/[^0-9 ]/))
			return false;

		// create array of numbers from input
		const numberSequence = input.replace(/ /g, '').split('');

		// every second number is doubled.
		// if the product is greater than 9, subtract 9
		const oddDigitsDoubled =
			numberSequence.reduceRight((array,value,index) => {
				if (index % 2 === 0)
					return [value * 1, ...array];
				else
					if (value * 2 < 10)
						return [value * 2, ...array];
					else
						return [value * 2 - 9, ...array];
			}, []);

		// sum all digits
		const sumOfDigits = oddDigitsDoubled.reduce((sum, num) => sum + num, 0);

		// valid if divisible into 10
		return sumOfDigits % 10 === 0;
	}
}
