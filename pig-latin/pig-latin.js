export class translator {
	static translate(word) {
		if (word.split(" ").length > 1) {
			let out = "";

			for (let s of word.split(" ")) out += this.translate(s) + " ";

			return out.trim();
		}

		if (word.match("^(ch|qu|the|rh).*")) {
			return word.substring(2) + word.substring(0, 2) + "ay";
		}

		if (word.match("^(sch|squ|thr).*")) {
			return word.substring(3) + word.substring(0, 3) + "ay";
		}

		if ("aeiou".includes(word[0]) || word.match("^(x|y)(?!e).*")) {
			return word + "ay";
		}

		return word.substring(1) + word[0] + "ay";
	}
}
