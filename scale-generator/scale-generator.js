//
// This is only a SKELETON file for the 'Scale Generator' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

const chromaticSharps = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];
const chromaticFlats = ['F', 'Gb', 'G', 'Ab', 'A', 'Bb', 'B', 'C', 'Db', 'D', 'Eb', 'E'];

export class Scale {
  constructor(tonic) {
    this.tonic = tonic;
  }

  chromatic() {
    const positionInChromaticFlats = chromaticFlats.indexOf(this.tonic);
    const positionInChromaticSharps = chromaticSharps.indexOf(this.tonic);
    if (positionInChromaticFlats < positionInChromaticSharps) {
      return [...chromaticFlats.slice(positionInChromaticFlats), ...chromaticFlats.slice(0, positionInChromaticFlats)]
    } else {
      return [...chromaticSharps.slice(positionInChromaticSharps), ...chromaticSharps.slice(0, positionInChromaticSharps)]
    }
  }

  interval(intervals) {
    throw new Error("Remove this statement and implement this function");
  }
}
