export function translate(rnaSequence="") {
	let remit = [];

	// process rnaSequence string by 3 char increments
	for (let offset = 0; offset < rnaSequence.length; offset += 3) {
		const codon = rnaSequence.substring(offset, offset + 3);

		switch (codon) {
			case "AUG":
				remit.push("Methionine");
				break;

			case "UUU": case "UUC":
				remit.push("Phenylalanine");
				break;

			case "UUA": case "UUG":
				remit.push("Leucine");
				break;

			case "UCU": case "UCC": case "UCA": case "UCG":
				remit.push("Serine");
				break;

			case "UAU": case "UAC":
				remit.push("Tyrosine");
				break;

			case "UGU": case "UGC":
				remit.push("Cysteine");
				break;

			case "UGG":
				remit.push("Tryptophan");
				break;

			case "UAA": case "UAG": case "UGA":
				return remit; // STOP

			default:
				throw new Error("Invalid codon");
		}

	}

	return remit;
}
