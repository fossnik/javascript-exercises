export class House {
	static verse(v) {
		const verb = [
			"",
			"lay in the house that Jack built.",
			"ate",
			"killed",
			"worried",
			"tossed",
			"milked",
			"kissed",
			"married",
			"woke",
			"kept",
			"belonged to"
		];

		const noun = [
			"",
			" the house that Jack built.",
			" the malt",
			" the rat",
			" the cat",
			" the dog",
			" the cow with the crumpled horn",
			" the maiden all forlorn",
			" the man all tattered and torn",
			" the priest all shaven and shorn",
			" the rooster that crowed in the morn",
			" the farmer sowing his corn",
			" the horse and the hound and the horn",
		];

		let remit = ["This is" + noun[v]];

		while (v-- > 1) {
			const verbElement = verb[v];
			const nounElement = noun[v];

			remit.push(`that ${verbElement}${v > 1 ? nounElement : ''}`);
		}

		return remit;
	}

	static verses(start, end) {
		function intSequence(start, end, n = start, arr = []) {
			return n === end ? arr.concat(n)
				: intSequence(start, end, start < end ? n + 1 : n - 1, arr.concat(n));
		}

		return intSequence(start, end)
			.flatMap(v => ["", ...this.verse(v)])
			.slice(1);
	}
}


