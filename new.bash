#!/usr/bin/env bash

if [ ! -d node_modules ]; then
  echo "The point of this is really to avoid installing node packages in each exercise"
  echo "Run this script from the folder where node_modules was last"
  exit 1
fi

new_exercise=$1

exercism download --exercise=$new_exercise --track=javascript >> /tmp/exercism_script.log 2>&1

new_exercise_dir=$(tail -1 /tmp/exercism_script.log)

if [ -d $new_exercise_dir ]; then
  mv node_modules/ package-lock.json $new_exercise_dir
  touch $new_exercise_dir/$new_exercise.js
  tail -3 /tmp/exercism_script.log
else
  echo "Didn't work"
  echo "Log:"
  cat /tmp/exercism_script.log
  exit 1
fi
