const normalize = word => word.toLowerCase().replace(/\W/g, '');

function tallyLetters(word) {
	let letterTally = {};

	for (const c of word)
		if (letterTally[c])
			letterTally[c]++;
		else
			letterTally[c] = 1;

	return letterTally;
}

function isObjectEqual(tallyA, tallyB) {
	for (const count in tallyA)
		if (tallyA[count] !== tallyB[count])
			return false;

	return true;
}

export class Anagram {
	constructor(input) {
		// convert to lowercase and remove non-alpha characters
		this.keyword = normalize(input);
		this.keywordTally = tallyLetters(this.keyword);
	}
	matches(words) {
		return words.filter(word => {
			word = normalize(word);

			// words must be same length
			if (word.length !== this.keyword.length)
				return false;

			// word shouldn't match itself
			if (word === this.keyword)
				return false;

			return isObjectEqual(tallyLetters(word), this.keywordTally);
		});
	}
}
