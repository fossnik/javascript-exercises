const isEqual = (a, b) =>
	a.length === b.length
		&& a.every((value, index) => value === b[index])
		&& 'EQUAL'
		|| false;

const isSublist = (a, b) => {
	// empty list is sublist of non-empty list
	if (a.length === 0)
		return 'SUBLIST';

	// get all indexes in list B that contain the first value of list A
	const potentialListHeads =
		b.reduce((listOfIndexes, char, index) =>
			(char === a[0])
				? [index, ...listOfIndexes]
				: listOfIndexes
			, []);

	// test above indexes of list B to see if any begin complete sublist of A
	if (potentialListHeads.some(i => isEqual(a, b.slice(i, i + a.length))))
		return 'SUBLIST';

	return false;
};

// swap inputs to "isSublist"
const isSuperlist = (a, b) =>
	isSublist(b, a)
		&& 'SUPERLIST'
		|| false;

export class List {
	constructor(input=[]) {
		this.list = input;
	}

	compare(b) {
		return isEqual(this.list, b.list)
			|| isSublist(this.list, b.list)
			|| isSuperlist(this.list, b.list)
			|| 'UNEQUAL';
	}
}
