const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

// return first "Monday" from a given month of a year (or another named day)
const firstMonday = (year, month, ordinalDayOfWeek) => [1,2,3,4,5,6,7]
	.filter(day => new Date(year, month, day).getDay() === ordinalDayOfWeek)[0];

const allMondaysInGivenMonth = (year, month, ordinalDay) => {
	let dayOfMonth = firstMonday(year, month, ordinalDay);
	let date = new Date(year, month, dayOfMonth);
	let dates = [];

	do {
		dates.push(new Date(date));
		dayOfMonth += 7;
		date.setDate(dayOfMonth);
	}
	while (date.getMonth() === month);

	return dates;
};

export const meetupDay = (year, month, weekdayToFind, criteria) => {
	const allMondays =
		allMondaysInGivenMonth(year, month, weekDays.indexOf(weekdayToFind));

	if (Number.parseInt(criteria.charAt(0)) < 5)
		return allMondays[Number.parseInt(criteria[0]) - 1];

	if (criteria === '5th' && allMondays[4])
		return allMondays[4];

	if (criteria === 'last')
		return allMondays[allMondays.length - 1];

	if (criteria === 'teenth')
		return allMondays.filter(d => d.getDate() >= 13 && d.getDate() <= 19)[0];

	throw new Error(undefined);
};
