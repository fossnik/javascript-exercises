export class HighScores {
	constructor(input) {
		this.scores = input;
	}

	get latest() {
		return this.scores[this.scores.length - 1];
	}

	get highest() {
		return Math.max(...this.scores);
	}

	get top() {
		// build a sorted array
		return this.scores.reduce((array, value) => {
			if (array.length === 0)                    // new array
				return [value];

			else if (value >= array[0])                // belongs at head of array
				return [value, ...array];

			else if (value <= array[array.length - 1]) // belongs at tail of array
				return [...array, value];

			// value belongs somewhere in the middle of the array
			return function findInsertionIndex(value, segOne, segTwo) {
				// BASE CASE - value fits between segment 1 and segment 2
				if ((value === segOne[segOne.length - 1] || value === segTwo[0])
				 || (value  <  segOne[segOne.length - 1] && value  >  segTwo[0]))
					return [...segOne, value, ...segTwo];

				// RECURSION CASE - test insertion at the next index
				// remove first element from segment 2 and add it to end of segment 1
				return findInsertionIndex(value, [...segOne, segTwo.shift()], segTwo);
			} (value, array.slice(0, 1), array.slice(1))
		}, [])
			.slice(0, 3); // take first 3 scores
	}

	get report() {
		return this.latest >= this.highest
			? `Your latest score was ${this.latest}. That's your personal best!`
			: `Your latest score was ${this.latest}. ` +
			  `That's ${this.highest - this.latest} short of your personal best!`;
	}
}
