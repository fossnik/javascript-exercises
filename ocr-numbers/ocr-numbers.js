export function convert(input) {
	function matchDigit(numitron) {
		switch (numitron) {
			case ' _ \n'
			+    '| |\n'
			+    '|_|\n':
				return '0';

			case '   \n'
			+    '  |\n'
			+    '  |\n':
				return '1';

			case ' _ \n'
			+    ' _|\n'
			+    '|_ \n':
				return '2';

			case ' _ \n'
			+    ' _|\n'
			+    ' _|\n':
				return '3';

			case '   \n'
			+    '|_|\n'
			+    '  |\n':
				return '4';

			case ' _ \n'
			+    '|_ \n'
			+    ' _|\n':
				return '5';

			case ' _ \n'
			+    '|_ \n'
			+    '|_|\n':
				return '6';

			case ' _ \n'
			+    '  |\n'
			+    '  |\n':
				return '7';

			case ' _ \n'
			+    '|_|\n'
			+    '|_|\n':
				return '8';

			case ' _ \n'
			+    '|_|\n'
			+    ' _|\n':
				return '9';

			default: return '?';
		}
	}

	function getDigitsFromSegment(segment) {
		const lines = segment.split('\n');

		let digitString = '';

		for (let index = 0; index < lines[0].length; index += 3) {
			const threeByThreeChar = lines[0].substr(index, 3) + '\n' +
				lines[1].substr(index, 3) + '\n' +
				lines[2].substr(index, 3) + '\n';

			digitString += matchDigit(threeByThreeChar);
		}

		return digitString;
	}

	const verticalSegments = input.split("         \n");
	return verticalSegments.map(
		segment => {
			console.log("numitron segment is: \n" + segment);

			const digitsFromSegment = getDigitsFromSegment(segment);
			console.log("recognized as: \n" + digitsFromSegment);

			return digitsFromSegment;
		}
	).join(',');
}
