const keyGen = () => Array.from({length: 100},() =>
	String.fromCharCode(Math.floor(Math.random() * 26) + 97)).join('');

export class Cipher {
	constructor(key=keyGen()) {
		// test validity of a provided key (lowercase letters exclusively)
		if (!key.match(/^[a-z]+$/))
			throw new Error('Bad key');

		this.key = key;
	}

	encode(input) {
		return input.split('')
			.reduce((remit, v, i) =>
				remit + String.fromCharCode((
					// normalized index of input character ('a' is 97 in ASCII)
					input.charCodeAt(i) - 97

					// addition shift (normalized index of key character)
					+ this.key.charCodeAt(i % this.key.length) - 97)

					// bounded by the length of the character set
					% 26

					// reverse normalization (back to ASCII)
					+ 97)
				, '');
	}

	decode(input) {
		return input.split('')
			.reduce((remit, v, i) =>
				remit + String.fromCharCode((
					// normalized index of input character ('a' is 97 in ASCII)
					input.charCodeAt(i) - 97

					// subtraction shift (normalized index of key character)
					- (this.key.charCodeAt(i % this.key.length) - 97)

					// bounded by length of character set (avoid potential negative values)
					+ 26) % 26

					// reverse normalization (back to ASCII)
					+ 97)
				, '');
	}
}
