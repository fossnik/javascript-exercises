export function GradeSchool() {
	const kids = {};

	return {
		roster: () => kids,
		add: (name, grade) =>
			kids[grade] =
				[...kids[grade] || [], name].sort(),
		grade: (grade) => kids[grade] || []
	};
}
