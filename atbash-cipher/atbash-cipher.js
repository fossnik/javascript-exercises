// extend the string object to filter out whitespace and shift to lower case
String.prototype.filterInput = function() {
	return this.replace(/\W/g, '').toLowerCase();
};

// extend the array object to return a string grouped into segments of five
Array.prototype.groupIntoFives = function() {
	return this.reduce((remit, char, index) =>
		index % 5 === 0
			? remit += ' ' + char
			: remit += char
	, '').trim();
};

function atbashEncodeChar(c) {
	// return number as-is
	if (!isNaN(c))
		return c;

	// get zero-index index of the input letter (ie. a = 0, z = 25)
	const zeroIndex = c.charCodeAt(0) - 'a'.charCodeAt(0);

	// get the transposed letter (bounded by size of alphabet)
	const newLetter = (26 - zeroIndex + 25) % 26;

	// transpose back into ASCII range (ie. a = 97, z = 122)
	return String.fromCharCode(newLetter + 'a'.charCodeAt(0));
}

export const encode = input =>
	input.filterInput()
		.split('')
		.map(c => atbashEncodeChar(c))
		.groupIntoFives();
