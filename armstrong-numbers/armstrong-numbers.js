export function validate(input) {
	const power = String(input).length;

	let sum = 0;
	for (const c of String(input)) {
		sum += Math.pow(Number(c), power);
	}

	return sum === input;
}
