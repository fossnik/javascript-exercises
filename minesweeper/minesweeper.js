export function annotate(input) {
	function countAdjacentMines(y, x) {
		let mineStr = String(input[y].substring(x - 1, x + 2));

		if (input[y - 1]) mineStr += input[y - 1].substring(x - 1, x + 2);
		if (input[y + 1]) mineStr += input[y + 1].substring(x - 1, x + 2);

		const numMines = [...mineStr]
			.filter(c => c === '*')
			.length;

		return numMines > 0 ? numMines : ' ';
	}

	return input.map((line, y) => [...line].map((_, x) =>
		input[y][x] === '*'
			? '*'
			: countAdjacentMines(y, x)
	).join(''));
}
