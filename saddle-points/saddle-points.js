export class Matrix {
	constructor(input) {
		this.rows = input
			.split('\n').map(row => row.trim()
				.split(' ').map(Number));

		// iterate from number 0 to the number of first row size
		this.columns = Array(this.rows[0].length).fill()
			.map((e, index) =>
				this.rows.map(column => column[index]));
	}

	get saddlePoints() {
		const allSaddlePoints = this.rows
			.reduce((saddlesArray, rowArray, rowIndex) => {
				const largestElementInRow = Math.max(...rowArray);

				const saddlePoints =
					rowArray.reduce((array, number, columnIndex) =>
						number === largestElementInRow &&
						number === Math.min(...this.columns[columnIndex])
							? [...array, [rowIndex, columnIndex]]
							: array
					, []);

				return saddlePoints.length > 0
					? [...saddlesArray, saddlePoints]
					: saddlesArray
			}, []);

		return allSaddlePoints.map(e => e[0]);
	}
}
