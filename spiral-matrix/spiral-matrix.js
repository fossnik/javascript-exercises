export class SpiralMatrix {
	static ofSize(edgeSize) {

		// create matrix of N by N size - 0 filled
		let remit = new Array(edgeSize);
		for (let i = 0; i < remit.length; i++) {
			remit[i] = new Array(edgeSize).fill(null);
		}

		for (let vector = new Direction(), i = 1, x = -1, y = 0, mustTurn = false;
		     i <= edgeSize ** 2;
		     mustTurn = false)
		{
			// horizontal movement
			if (vector.direction % 2 === 0) {
				if (x + vector.x < 0 || x + vector.x > edgeSize - 1) mustTurn = true; // out of bounds
				else if (remit[y][x + vector.x] !== null) mustTurn = true; // already filled space
				else x += vector.x; // continue iteration along this vector

			// vertical movement
			} else {
				if (y + vector.y < 0 || y + vector.y > edgeSize - 1) mustTurn = true;
				else if (remit[y + vector.y][x] !== null) mustTurn = true;
				else y += vector.y;
			}

			if (mustTurn) vector.nextCardinalDirection();
			else remit[y][x] = i++;
		}

		return remit;
	}
}

const cardinals = {
	0: {'x':  1, 'y':  0},
	1: {'x':  0, 'y':  1},
	2: {'x': -1, 'y':  0},
	3: {'x':  0, 'y': -1}
};

class Direction {
	constructor() {
		this.direction = 0;
		this.x = cardinals[this.direction].x;
		this.y = cardinals[this.direction].y;
	}

	nextCardinalDirection() {
		this.direction = (this.direction + 1) % 4;
		this.x = cardinals[this.direction].x;
		this.y = cardinals[this.direction].y;
	}
}
