const orbitalSeconds = {
	mercury:    7600544,
	venus:     19414149,
	earth:     31557600,
	mars:      59354032,
	jupiter:  374355659,
	saturn:   929292363,
	uranus:  2651370019,
	neptune: 5200418560,
};

export const age = (planet, age) => Number((age / orbitalSeconds[planet]).toFixed(2));
