const Allergen = {
	1: 'eggs',
	2: 'peanuts',
	4: 'shellfish',
	8: 'strawberries',
	16: 'tomatoes',
	32: 'chocolate',
	64: 'pollen',
	128: 'cats',
};

export const Allergies = function (input) {
	this.allergies = [];

	// this binary number is a T/F value for each allergen (read right to left)
	parseInt(input, 10).toString(2)

	// create array, reverse order, and iterate through each element with map
		.split('').reverse().map((isTrue, index) =>

			// if the index in the binary string indicates true, and
			isTrue === '1' &&

			// there does exist a value in the allergen index, then
			Allergen[Math.pow(2, index)] &&

			// add it to the array of allergies
			this.allergies.push(Allergen[Math.pow(2, index)])
	);
};

Allergies.prototype = {
	allergicTo: function (input) {
		return this.allergies.includes(input);
	},
	list: function () {
		return this.allergies;
	}
};
