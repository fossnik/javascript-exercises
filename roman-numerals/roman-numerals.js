export function toRoman(digits) {
	let romanNumerals = "";
	const decimals = [ 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 ];
	const numerals = [ "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" ];

	// keep subtracting amounts from largest to smallest
	for (let i = 0; i < decimals.length; i++) {
		while ( digits >= decimals[i] ) {
			romanNumerals = romanNumerals + numerals[i];
			digits -= decimals[i];
		}
	}

	return romanNumerals;
}
