export const convert = input => `${input % 3 === 0 ? 'Pling' : ''}${input % 5 === 0 ? 'Plang' : ''}${input % 7 === 0 ? 'Plong' : ''}` || String(input);
