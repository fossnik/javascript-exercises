const brackets = new Map([
	['[', ']'],
	['{', '}'],
	['(', ')'],
]);

export const bracketPush = (input) => {
	let queue = [];

	for (const c of input)
		if (Array.from(brackets.keys()).includes(c))
			queue.push(c);
		else if (Array.from(brackets.values()).includes(c) && brackets.get(queue.pop()) !== c)
			return false;

	return queue.length === 0;
};
