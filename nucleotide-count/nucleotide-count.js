export const NucleotideCounts = {
	parse: function (input) {
		const tally = {'A': 0, 'C': 0, 'G': 0, 'T': 0};

		for (const c of input)
			if (isNaN(tally[c]++))
				throw Error('Invalid nucleotide in strand');

		return `${tally['A']} ${tally['C']} ${tally['G']} ${tally['T']}`;
	}
};
