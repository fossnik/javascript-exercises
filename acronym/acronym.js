export const parse = input => input
	// split on spaces or dashes
	.split(/[ -]/)

	// take the first letter
	.map(word => word.charAt(0)

		// append subsequent uppercase letters from same word (camelCase)
		.concat(word.substring(1).replace(/[a-z]/g,''))

		// make all uppercase and re-join
		.toUpperCase()).join('');
