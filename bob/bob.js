const isAllCaps = phrase =>
	phrase.split('').every(letter => letter === letter.toUpperCase());

const containsLetters = phrase =>
	phrase.toUpperCase() !== phrase.toLowerCase();

export const hey = message => {
	// eliminate leading/trailing whitespace
	message = message.trim();

	if (containsLetters(message) && isAllCaps(message))
		if (message.endsWith("?"))
			return "Calm down, I know what I'm doing!";
		else
			return "Whoa, chill out!";

	if (message.endsWith("?"))
		return "Sure.";

	if (message.length < 1)
		return "Fine. Be that way!";

	return "Whatever.";
};
