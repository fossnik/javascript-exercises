export class Song {
	verse(v) {
		const chain =  ["fly", "spider", "bird", "cat", "dog", "goat", "cow", "horse"];
		const refrain = [ "",
			"It wriggled and jiggled and tickled inside her.\n",
			"How absurd to swallow a bird!\n",
			"Imagine that, to swallow a cat!\n",
			"What a hog, to swallow a dog!\n",
			"Just opened her throat and swallowed a goat!\n",
			"I don't know how she swallowed a cow!\n",
			"She's dead, of course!"
		];

		if (v === 8)
			return "I know an old lady who swallowed a horse.\nShe's dead, of course!\n";

		v--; // correct zero-index misalignment

		// always start with "I know an old lady who swallowed a ...", and then a specific refrain
		let remit = `I know an old lady who swallowed a ${chain[v]}.\n${refrain[v]}`;

		// after the specific refrain, iterate through the whole chain of animals
		while (v-- > 0)
			if (v === 1) // the "wriggle and jiggle and tickling spider" is kind of an anomalous line
				remit += "She swallowed the bird to catch the spider that wriggled and jiggled and tickled inside her.\n";
			else
				remit += `She swallowed the ${chain[v+1]} to catch the ${chain[v]}.\n`;

		// end with the outtro "I don't know why..."
		return remit + "I don't know why she swallowed the fly. Perhaps she'll die.\n";
	}

	verses(start, end) {
		let remit = "";

		while (start++ <= end) remit += this.verse(start - 1) + "\n";

		return remit;
	}
}
