export class Matrix {
	constructor(input) {
		this.rows = input
			.split('\n').map(row => row
				.split(' ').map(num => Number(num)));

		// iterate from number 0 to the number of first row size
		this.columns = this.rows[0]
			.map((rowElement, index) =>
				this.rows.map(column => column[index]));
	}
}
