export const transpose = input => {
	let rows = [];

	// longest element in array
	const longest = Math.max(...(input.map(e => e.length)));

	for (let i = 0; i < longest; i++)
		rows[i] = input
			.map(element => element.charAt(i) || ' ')
			.join('');

	// ARGH! padding!

	return rows;
};

