const bigInt = require('./lib/big-integer');

export class Grains {
	square(number) {
		let output = '1';

		for (let i = 1; i < number; i++)
			output = bigInt(output).multiply(2);

		return bigInt(output).value.toString();
	}
}
