// return sequence 0..length
const iterableSequence = length =>
	Array
		.apply(null, {length})
		.map(Array.call, Number);

const generateRow = (length, masterArray) =>
	iterableSequence(length + 1)
		.reduce((rowArray, value) => {
			const lastRow = masterArray[masterArray.length - 1] || [1];
			const aboveLeft = lastRow[value - 1] || 0;
			const aboveRight = lastRow[value] || 0;

			return [...rowArray, aboveLeft + aboveRight];
		}
		, []);

export class Triangle {
	constructor(length) {
		this.rows = iterableSequence(length)
			.reduce((masterArray, value) => [
				...masterArray,
				generateRow(value, masterArray)
			], []);

		this.lastRow = this.rows[this.rows.length - 1];
	}
}
