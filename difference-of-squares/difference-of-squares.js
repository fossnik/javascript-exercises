export class Squares {
  constructor(input) {
    this.input = input;
  }

  // here are demonstrated two different means of working with Array
  get squareOfSum() {
    return Array.apply(null, {length: this.input})
        .map(Array.call, Number)
        .reduce((accumulator, value) =>
            accumulator += (value + 1)
      , 0) ** 2;
  }

  get sumOfSquares() {
    return Array.from({length: this.input}, (value, index) => index)
        .reduce((accumulator, value) =>
            accumulator += (value + 1) ** 2
      , 0);
  }

  get difference() {
    return this.squareOfSum - this.sumOfSquares;
  }
}
