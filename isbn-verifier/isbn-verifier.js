const ISBN = class {
  constructor(input) {
    this.input = input;
  }

  isValid() {
    // the only characters should be numbers, dashes, and 'X'
    if (this.input.split("").some(c => c.match("[^0-9X\-]"))) return false;

    // drop non-digit characters other than 'X' (the check character)
    let digits = this.input.split("").filter(c => c === 'X' || c >= 0).join("");

    // must have 9 or 10 digits
    if (digits.length !== 10 && digits.length !== 9) return false;

    // sum according to the function
    const sum = digits.split("")
          .map(c => c === 'X' ? 10 : c)
          .reduceRight((remit, c, index) =>
              remit += c * (10 - index)
          , 0);

    return sum % 11 == 0;
  }
}

module.exports = {ISBN};
