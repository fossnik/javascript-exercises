export const largestProduct = (string, width) => {

	// negative width
	if (width < 0)
		throw new Error('Invalid input.');

	// includes non-digit characters
	if (string.match(/\D/))
		throw new Error('Invalid input.');

	// width larger than input
	if (width > string.length)
		throw new Error('Slice size is too big.');

	let largest = 0;

	for (let i = 0; i <= string.length - width; i++) {
		const segment = string.substring(i, i + width).split('');
		const product = segment.reduce((product, num) => product * num, 1);

		if (product > largest)
			largest = product;
	}

	return largest;
};
