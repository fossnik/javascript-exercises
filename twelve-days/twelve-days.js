export class TwelveDays {
	printVerse(verseNumber) {
		const ordinals = ["zeroeth", "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth"];
		let remit = `On the ${ordinals[verseNumber]} day of Christmas my true love gave to me: `;

		switch(verseNumber) {
			case 12: 	remit += "twelve Drummers Drumming, ";
			case 11: 	remit += "eleven Pipers Piping, ";
			case 10: 	remit += "ten Lords-a-Leaping, ";
			case 9:		remit += "nine Ladies Dancing, ";
			case 8:		remit += "eight Maids-a-Milking, ";
			case 7:		remit += "seven Swans-a-Swimming, ";
			case 6:		remit += "six Geese-a-Laying, ";
			case 5:		remit += "five Gold Rings, ";
			case 4:		remit += "four Calling Birds, ";
			case 3:		remit += "three French Hens, ";
			case 2:		remit += "two Turtle Doves, ";
			case 1:
				if (verseNumber === 1) remit += "a Partridge in a Pear Tree.\n";
				else remit += "and a Partridge in a Pear Tree.\n";
		}

		return remit;
	}

	verse(startVerse, endVerse=startVerse) {
		let remit = "";

		while (startVerse < endVerse)
			remit += this.printVerse(startVerse++) + "\n";

		return remit + this.printVerse(endVerse);
	}

	sing() {
		return this.verse(1, 12);
	}
}
