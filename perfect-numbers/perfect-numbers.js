export function classify(input) {
	if (input < 1)
		throw new Error('Classification is only possible for natural numbers.');

	let aliquotSum = 0;

	// sum factors of the input number
	for (let i = input / 2; i > 0; i--)
		if (input % i === 0)
				aliquotSum += i;

	if (aliquotSum === input)
		return 'perfect';

	if (aliquotSum > input)
		return 'abundant';

	if (aliquotSum < input)
		return 'deficient';
}
