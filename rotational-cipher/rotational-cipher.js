const rotateChar = (char, rot) => {
	// lowercase letter
	if (char.match(/[a-z]/))
		var asciiZeroIndex = 'a'.charCodeAt(0);

	// uppercase letter
	else if (char.match(/[A-Z]/))
		var asciiZeroIndex = 'A'.charCodeAt(0);

	// return non-alpha as-is
	else return char;

	// get the zero-adjusted index of the letter (ie. a|A = 0, z|Z = 25)
	const normalizedLetterIndex = char.charCodeAt(0) - asciiZeroIndex;

	// get the transposed letter (bounded by size of alphabet)
	const newLetter = (normalizedLetterIndex + rot) % 26;

	// transpose back into ASCII range (ie. a = 97, A = 65)
	return String.fromCharCode(newLetter + asciiZeroIndex);
};

export class RotationalCipher {
	static rotate(input, rot) {
		return input.split('').map(char => rotateChar(char, rot)).join('')
	}
}
