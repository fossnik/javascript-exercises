export class Words {
	count(input) {
		const wordTally = {};

		// make input lowercase
		input = input.toLowerCase();

		// replace any arbitrary whitespace with a single space (for delimiting)
		input = input.replace(/\s+/g, ' ').trim();

		// iterate through space-delimited input array
		for (const word of input.split(' '))
			if (word === 'constructor')
				wordTally['constructor'] = 1;
			else if (wordTally[word])
				wordTally[word]++;
			else
				wordTally[word] = 1;

		return wordTally;
	}
}
