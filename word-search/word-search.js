function intSequence(start, end, n = start, arr = []) {
	return n === end ? arr.concat(n)
		: intSequence(start, end, start < end ? n + 1 : n - 1, arr.concat(n));
}

export default class WordSearch {
	constructor(grid) {
		this.yMax = grid.length;
		this.xMax = grid[0].length;
		this.searchMatrix = grid;
	}

	find(words) {
		let remit = {};
		for (const word of words) {
			let starts = this.findFirstLetter(word[0]);
			for (const start of starts) {
				const ends = this.findLastLetter(start, word);
				for (const end of ends) {
					remit[word] = this.validateWordExistsAtVector(start, end, word)
						? {start: [start.y + 1, start.x + 1], end: [end.y + 1, end.x + 1]}
						: undefined;
				}
			}
		}
		return remit;
	}

	validateWordExistsAtVector(start, end, word) {
		const yVector = intSequence(start.y, end.y);
		const xVector = intSequence(start.x, end.x);
		const yLen = yVector.length;
		const xLen = xVector.length;

		return [...word].every((letter, i) =>
			this.searchMatrix[yVector[i%yLen]][xVector[i%xLen]] === letter);
	}


	findLastLetter(start, word) {
		const length = word.length - 1;
		const lastLetter = word[length];
		const {y, x} = start;
		const potentialEndpoints = [
			{y: y         , x: x + length},
			{y: y         , x: x - length},
			{y: y + length, x: x         },
			{y: y - length, x: x         },
			{y: y + length, x: x + length},
			{y: y + length, x: x - length},
			{y: y - length, x: x + length},
			{y: y - length, x: x - length}
		];

		return potentialEndpoints.filter(({y, x}) =>
			this.searchMatrix.hasOwnProperty(y)
				&& this.searchMatrix[y][x] === lastLetter);
	}

	findFirstLetter(letter) {
		let remit = [];
		for (let y = 0; y < this.yMax; y++) {
			for (let x = 0; x < this.xMax; x++) {
				if (this.searchMatrix[y][x] === letter) {
					remit.push({y, x});
				}
			}
		}
		return remit;
	}
}
