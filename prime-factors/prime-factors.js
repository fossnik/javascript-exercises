export function primeFactors(input) {
	let remit = [];
	let divisor = 2;
	let number = input;

	while (number > 1) {
		while (number % divisor === 0) {
			remit.push(divisor);
			number = number / divisor;
		}
		divisor++;
	}

	return remit;
}
