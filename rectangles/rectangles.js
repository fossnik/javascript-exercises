export class Rectangles {
	static count(input) {
		let rectangles = new Set();

		for (let y = 0; y < input.length; y++) {
			for (let x = 0; x < input[0].length; x++) {
				testCell(y, x);
			}
		}

		function testCell(y, x) {
			if (input[y][x] === "+") {
				moveRight({y, x}, null, null, null, {y, x: x + 1});
			}

			function moveRight(tl, tr, br, bl, {y, x}) {
				const cell = input[y][x];
				// noinspection FallThroughInSwitchStatementJS (this is intentional)
				switch (cell) {
					case "+":
						moveDown(tl, {y, x}, null, null, {y: y + 1, x});
					case "-":
						moveRight(tl, null, null, null, {y, x: x + 1});
				}
			}

			function moveDown(tl, tr, br, bl, {y, x}) {
				if (y < input.length) {
					const cell = input[y][x];
					// noinspection FallThroughInSwitchStatementJS
					switch (cell) {
						case "+":
							moveLeft(tl, tr, {y, x}, null, {y, x: x - 1});
						case "|":
							moveDown(tl, tr, null, null, {y: y + 1, x});
					}
				}
			}

			function moveLeft(tl, tr, br, bl, {y, x}) {
				const cell = input[y][x];
				// noinspection FallThroughInSwitchStatementJS (this is intentional)
				switch (cell) {
					case "+":
						moveUp(tl, tr, br, {y, x}, {y: y - 1, x});
					case "-":
						moveLeft(tl, tr, br, null, {y, x: x - 1});
				}
			}

			function moveUp(tl, tr, br, bl, {y, x}) {
				if (y >= 0) {
					const cell = input[y][x];
					// noinspection FallThroughInSwitchStatementJS
					switch (cell) {
						case "+":
							pushRectangle(tl, tr, br, bl);
						case "|":
							moveUp(tl, tr, br, bl, {y: y - 1, x});
					}
				}
			}

			function pushRectangle(tl, tr, br, bl) {
				if (tl.x === bl.x && tr.y === tl.y &&
					br.x === tr.x && br.y === bl.y) {
					rectangles.add(new Rectangle(tl, tr, br, bl).toString());
				}
			}
		}

		return rectangles.size;
	}
}

class Rectangle {
	constructor(tl, tr, br, bl) {
		this.topLeft = tl;
		this.topRight = tr;
		this.bottomRight = br;
		this.bottomLeft = bl;
	}

	toString() {
		return 'tl:' + this.topLeft.x + ',' + this.topLeft.y +
			'tr:' + this.topRight.x + ',' + this.topRight.y +
			'bl:' + this.bottomLeft.x + ',' + this.bottomLeft.y +
			'br:' + this.bottomRight.x + ',' + this.bottomRight.y;
	}
}
