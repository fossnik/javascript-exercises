export class LinkedList {
	constructor() {
		this.queue = new Array(0);
	}

	push(e) {
		return this.queue.push(e)
	}

	unshift(e) {
		return this.queue.unshift(e);
	}

	pop() {
		return this.queue.pop();
	}

	shift() {
		return this.queue.shift();
	}

	count() {
		return this.queue.length;
	}

	delete(e) {
		const index = this.queue.indexOf(e);
		if (index > -1)
			this.queue.splice(index, 1);

		return this.queue.length;
	}
}
