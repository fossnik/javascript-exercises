export function Triangle(a, b, c) {
  this.a = a;
  this.b = b;
  this.c = c;

  this.isEquilateral = () => a == b && b == c;
  this.isIsosceles = () => a == b || b == c || a == c;
  this.isScalene = () => a != b && b != c && a != c;
}

Triangle.prototype.kind = function () {
  if (this.a <= 0 || this.b <= 0 || this.c <= 0)
    throw new Error("No Negative Sides");
  if (this.a >= this.b + this.c || this.b >= this.a + this.c || this.c >= this.a + this.b)
    throw new Error("Triangle Inequality Violation");

  if (this.isEquilateral())
    return "equilateral";
  if (this.isIsosceles())
    return "isosceles";
  if (this.isScalene())
    return "scalene";
}
