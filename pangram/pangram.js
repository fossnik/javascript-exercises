export const isPangram = (input) =>

	// ES6 syntax to create a hash-set of unique values	(eliminate duplicates)
	[...new Set(

		// lowercase the input
		input.toLowerCase()

			// create an array from input
			.split("")

			// filter input to exclude non-alpha symbols
			.filter(value => value.match(/^[a-z]$/)))]

		// true if input contains 26 unique lowercase characters
		.length === 26;
