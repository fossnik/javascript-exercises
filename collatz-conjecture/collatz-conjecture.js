export const steps = n => {
  if (n < 1)
    throw new Error('Only positive numbers are allowed');
  for (let steps = 0;; steps++)
    if (n == 1)
      return steps;
    else if (n % 2 === 0)
      n = n / 2;
    else
      n = n * 3 + 1;
}
