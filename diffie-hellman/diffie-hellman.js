function isPrime(num) {
	for (let i = 2; i < num; i++)
		if (num % i === 0)
			return false;
	return num !== 1;
}

export class DiffieHellman {
	constructor(p, g) {
		if (p < 1 || g < 1)
			throw new Error('Private key is not greater than 1');

		if (!isPrime(p) || !isPrime(g))
			throw new Error('Argument not prime');

		this.p = p;
		this.g = g;
	}

	getPublicKeyFromPrivateKey(privateKey) {
		if (privateKey < 2)
			throw new Error('Less than 2')

		if (privateKey >= this.p)
			throw new Error("The private key is greater than public key")

		return Math.pow(this.g, privateKey) % this.p;
	}

	static getSharedSecret(privateKey, publicKey) {
		return Math.pow(publicKey, privateKey) % this.p;
	}
}
