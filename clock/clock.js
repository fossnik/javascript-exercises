class Clock {
	constructor(hh, mm = 0) {
		// put overflow minutes into the hours
		hh += Math.floor(mm / 60);

		// resolve negative values
		this.hrs = (hh % 24 + 24) % 24;
		this.min = (mm % 60 + 60) % 60;
	}

	plus(minutes) {
		return at(this.hrs, this.min + minutes);
	}

	minus(minutes) {
		return at(this.hrs, this.min - minutes);
	}

	equals(clockTwo) {
		return this.toString() === clockTwo.toString();
	}

	toString() {
		return `${('0' + this.hrs).slice(-2)}:${('0' + this.min).slice(-2)}`;
	}
}

export const at = (hour, minute) => new Clock(hour, minute);
