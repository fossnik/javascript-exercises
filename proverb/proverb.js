export function proverb() {
  let remit = "";

  // sometimes the last verse has a qualifier
  const qualifier = arguments[arguments.length - 1].qualifier || "";

  let words = [];
  for (const a in arguments) {
    words[a] = arguments[a];
  }
  words = words.filter(a => !a.qualifier);

  // The argument object contains an array of the arguments used when the function was called (invoked).
  for (let i = 0; i < words.length - 1;) {
    remit += "For want of a " + words[i] + " the " + words[++i] + " was lost.\n";
  }

  remit += "And all for the want of a " + (qualifier ? qualifier + " " : "") + words[0] + '.';

  return remit;
}
