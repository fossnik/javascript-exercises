export function convert(inputArray, baseFrom, baseTo) {
	if (baseFrom % 1 !== 0 || baseFrom < 2) throw new Error('Wrong input base');
	if (baseTo % 1 !== 0 || baseTo < 2) throw new Error('Wrong output base');
	if (inputArray.length === 0) throw new Error('Input has wrong format');
	if (inputArray[0] === 0 && inputArray.length > 1) {
		throw new Error('Input has wrong format');
	}
	if (inputArray.every(i => i === 0) && inputArray.length > 1) {
		throw new Error('Input has wrong format');
	}
	if (inputArray.find(i => i >= baseFrom || i < 0)) {
		throw new Error('Input has wrong format');
	}

	function getRealValue(inputArray, baseFrom) {
		return inputArray.reduceRight((accumulator, value, index) =>
			accumulator + value * baseFrom ** (inputArray.length - index - 1)
			, 0)
	}

	function getBaseRepresentationOfValue(realValue) {
		let remit = [];

		// find the most significant digit
		let largestDigit = 1;
		while (baseTo ** largestDigit < realValue) largestDigit++;

		// start from the most significant digit
		for (let i = largestDigit - 1; i >= 0; i--) {
			const baseFactor = baseTo ** i;

			if (realValue / baseFactor >= 0) {
				// how many times does it go into this base factor?
				const timesGoesIn = Math.floor(realValue / baseFactor);

				realValue -= baseFactor * timesGoesIn;

				remit.push(timesGoesIn);
			} else remit.push(0);
		}

		return remit;
	}

	const realValue = getRealValue(inputArray, baseFrom);

	return getBaseRepresentationOfValue(realValue);
}
