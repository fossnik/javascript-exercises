const getWords = (number) => {
	if (number > 99) {
		return [
			getWords(Math.floor(number / 100)),
			"hundred",
			getWords(number%100)
		].join(" ");
	}
	if (number > 20 && number % 10 !== 0) {
		const singleDigit = number % 10;
		const doubleDigit = number - singleDigit;
		return getWords(doubleDigit) + "-" + getWords(singleDigit);
	}
	switch (number) {
		case 0: return "";
		case 1: return "one";
		case 2: return "two";
		case 3: return "three";
		case 4: return "four";
		case 5: return "five";
		case 6: return "six";
		case 7: return "seven";
		case 8: return "eight";
		case 9: return "nine";
		case 10: return "ten";
		case 11: return "eleven";
		case 12: return "twelve";
		case 13: return "thirteen";
		case 14: return "fourteen";
		case 15: return "fifteen";
		case 16: return "sixteen";
		case 17: return "seventeen";
		case 18: return "eighteen";
		case 19: return "nineteen";
		case 20: return "twenty";
		case 30: return "thirty";
		case 40: return "forty";
		case 50: return "fifty";
		case 60: return "sixty";
		case 70: return "seventy";
		case 80: return "eighty";
		case 90: return "ninety";
		default: return "NOT FOUND";
	}
};

const factor = (len, index) => {
	const magnitude = len - 1 - index;
	return {
		0: "",
		1: "thousand",
		2: "million",
		3: "billion",
	}[magnitude];
};

class Say {
	inEnglish(input) {
		if (!(input >= 0 && input <= 999999999999)) {
			throw new Error("Number must be between 0 and 999,999,999,999.");
		}

		if (input === 0) return "zero";

		// splits into array of strings (3 characters each)
		// use padding to make evenly divisible (bitwise exclusive or)
		const pad = "0".repeat(((String(input).length-1)%3^3)-1);
		const inputArray = (pad + String(input)).match(/.{1,3}/g);

		return inputArray.reduceRight((remit, current, index) => [
				getWords(Number(current)),
				Number(current) > 0 ? factor(inputArray.length, index) : "",
				remit
			].filter(e => e.length > 0).join(" ")
			, "").trim();
	}
}

module.exports = {Say};
