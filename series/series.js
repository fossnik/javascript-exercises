export class Series {
	constructor(input) {
		this.digits = input.split('').map(c => parseInt(c));
	}
	slices(width) {
		var digits = this.digits; // 'this' is undefined in the 'getNextElements' function

		if (width > digits.length)
			throw new Error('Slice size is too big.');

		// use recursion to get the next N elements from the array, where N = width
		function getNextElements(index, diminish, remit) {
			// base case - the remit array contains N elements
			if (diminish === 0)
				return remit;

			// base case - proceeding would exceed the length of the digits array
			if (index + diminish > digits.length)
				return remit;

			// recursion case - proceed to next element
			return getNextElements(index + 1, diminish - 1, [...remit, digits[index]]);
		}

		// loop through each element in this.digits, adding N elements to an array, where N = width
		return digits.reduce((array, currentValue, index) =>
			[...array, getNextElements(index, width, [])]
			, [])
			    .filter(node => node.length !== 0); // filter out empty nodes
	}
}
