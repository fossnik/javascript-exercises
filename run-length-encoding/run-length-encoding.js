export function encode(input) {
	let output = "";

	let count = 1;
	let first = '';
	for (const next of input) {
		if (first === next) count++;
		else {
			output += count > 1 ? count + first : first;
			count = 1;
			first = next;
		}
	}

	return output += count > 1 ? count + first : first;
}

export function decode(input) {
	let output = '';

	for (let i = 0; i < input.length; i++) {
		if (input[i] > 0) {
			let count = input[i];
			while (input[++i] > 0) count += input[i];
			output += input[i].repeat(count);
		}
		else {
			output += input[i];
		}
	}

	return output;
}
