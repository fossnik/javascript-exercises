export const Triplet = function(a, b, c) {
  return {
    sum: function() { return a + b + c},
    product: function() { return a * b * c},
    isPythagorean: function() { return a ** 2 + b ** 2 === c ** 2}
  }
}
